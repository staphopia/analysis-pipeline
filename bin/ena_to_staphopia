#! /usr/bin/env python
"""
Process ENA experiment and insert the results into Staphopia.

Check directory
if file
    create sample
    create sample directory
    move file to new directory
    create job script
    submit job
else
    exit
"""
if __name__ == '__main__':
    import sys
    import glob
    from os.path import basename
    import argparse as ap

    from staphopia import ena
    from staphopia.config import PIPELINE_PATH

    parser = ap.ArgumentParser(
        prog='ena_to_staphopia',
        conflict_handler='resolve',
        description=(''))
    group1 = parser.add_argument_group('Options', '')
    group1.add_argument(
        '--input_dir', metavar="STRING", type=str, required=True,
        help='Directory of ENA downloads for submission to Staphopia.'
    )
    group1.add_argument('--output_dir', metavar="STRING", type=str,
                        required=True, help='Directory to submit jobs to.')
    group1.add_argument('-p', '--processors', metavar="INT", type=int,
                        help='Number of processors to use. (Default 1)',
                        default=1,)
    group1.add_argument('--production', action='store_true', default=False,
                        help='Use production server settings', )
    group1.add_argument('--debug', action='store_true', default=False,
                        help='Do not save results to database')

    group3 = parser.add_argument_group('Optional', '')
    group3.add_argument('-h', '--help', action='help',
                        help='Show this help message and exit')

    if len(sys.argv) == 1:
        parser.print_usage()
        sys.exit(1)

    args = parser.parse_args()

    config = {
        'n_cpu': str(args.processors),
        'production': args.production,
        'debug': args.debug,
        'settings': 'staphopia.settings.{0}'.format(
            'prod' if args.production else 'dev'
        ),
        'create_job_script': PIPELINE_PATH + '/create_job_script',
        'manage': '/home/rpetit/staphopia/staphopia.com/manage.py',
    }

    files = glob.glob('{0}/*.fastq.gz'.format(args.input_dir))
    if files:
        ena_obj = ena.ENA(config)
        for file in files:
            experiment = basename(file).replace('.fastq.gz', '')
            is_paired = ena_obj.is_paired(experiment)
            ena_to_sample = ena_obj.ena_to_sample(experiment, is_paired)
            if ena_to_sample:
                outdir = '{0}/{1}/{2}'.format(
                    args.output_dir,
                    ena_to_sample['username'],
                    ena_to_sample['sample_tag']
                )

                new_file = '{0}/{1}.fastq.gz'.format(
                    outdir,
                    ena_to_sample['sample_tag']
                )
                job_script = new_file.replace('fastq.gz', 'sh')
                if ena_obj.move_experiment(file, new_file, outdir):
                    if ena_obj.create_job_script(job_script, new_file, outdir,
                                                 is_paired, ena_to_sample):

                        ena_obj.submit_job(job_script)
                    else:
                        print 'Failed to create {0}'.format(job_script)
                else:
                    print 'Failed to move {0}'.format(new_file)
            else:
                print 'Failed to load {0} into Staphopia'.format(experiment)
    else:
        print 'nothing to do!'
