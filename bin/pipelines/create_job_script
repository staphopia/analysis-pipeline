#! /usr/bin/python
"""
Create a Staphopia job script.

usage: create_job_script [--input STRING] [--working_dir STRING] [--paired]
                         [--sample_tag STRING] [--processors INT] [-h]
                         [--version]

Example Usage: create_job_script --input /path/to/reads.fastq.gz --paired
"""

if __name__ == '__main__':
    import sys
    import argparse as ap

    from staphopia.config import *

    parser = ap.ArgumentParser(
        prog='create_job_script',
        conflict_handler='resolve',
        description=('Create a SGE usable script to submit a FASTQ file '
                     'through the pipeline. Print to STDOUT.'))
    group1 = parser.add_argument_group('Options', '')
    group1.add_argument('--input', metavar="STR", type=str, default=False,
                        help='Input FASTQ file')
    group1.add_argument('--working_dir', metavar="STR", type=str, default='./',
                        help='Working directory to execute script from.')
    group1.add_argument('--processors', metavar="INT", type=int, default=1,
                        help='Number of processors to use.')
    group1.add_argument('--sample_tag', metavar="STR", type=str, default=False,
                        help='Optional: Sample tag of the input')
    group1.add_argument('--paired', action='store_true', default=False,
                        help='Input is interleaved paired end reads.')
    group1.add_argument('--log_times', action='store_true', default=False,
                        help='Write task run times to file (Default: STDERR).')
    group3 = parser.add_argument_group('Help', '')
    group3.add_argument('-h', '--help', action='help',
                        help='Show this help message and exit')
    group3.add_argument('--version', action='version', version='%(prog)s v0.1',
                        help='Show program\'s version number and exit')

    if len(sys.argv) == 1:
        parser.print_usage()
        sys.exit(1)

    args = parser.parse_args()

    # Job Constants -----------------------------------------------------------
    IS_PAIRED = '--paired' if args.paired else ''
    SAMPLE_TAG = '--sample_tag ' + args.sample_tag if args.sample_tag else ''
    LOG_TIMES = '--log_times' if args.log_times else ''

    JOB_SCRIPT = '\n'.join([
        '#! /bin/bash',
        '#$ -wd {0}'.format(args.working_dir),
        '#$ -V',
        '#$ -N j{0}'.format(args.sample_tag),
        '#$ -S /bin/bash',
        '#$ -pe orte {0}'.format(args.processors),
        '#$ -o {0}/submit_job.stdout'.format(args.working_dir),
        '#$ -e {0}/submit_job.stderr'.format(args.working_dir),
        '',
        '# Environment Variables',
        'export PATH={0}:{1}:{2}:$PATH'.format(
            PATH, PIPELINE_PATH, THIRD_PARTY_PATH
        ),
        'export PYTHONPATH={0}:{1}:{2}:$PYTHONPATH'.format(
            BASE_DIR, PYTHON_REQS, VCFANNOTATOR
        ),
        'export OMP_NUM_THREADS={0}'.format(
            (1 if args.processors - 1 == 0 else args.processors - 1)
        ),
        'export OMP_THREAD_LIMIT={0}'.format(args.processors),
        '',
        '# Command',
        '',
        '{0}/submit_job -i {1} -p {2} {3} {4} {5}'.format(
            PIPELINE_PATH,
            args.input,
            args.processors,
            IS_PAIRED,
            SAMPLE_TAG,
            LOG_TIMES
        ),
        '',
    ])

    print JOB_SCRIPT
